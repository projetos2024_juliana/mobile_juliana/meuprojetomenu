import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import TelaInicial from './src/layout/telainicial';
import SegundaTela from './src/layout/segundatela';
import TerceiraTela from './src/layout/terceiraTela';
import QuartaTela from './src/layout/quartaTela';
import QuintaTela from './src/layout/quintaTela';
import Menu from './src/menu';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>

        <Stack.Screen name="Menu" component={Menu}/>
        <Stack.Screen name="TelaInicial" component={TelaInicial}/>
        <Stack.Screen name="SegundaTela" component={SegundaTela}/>
        <Stack.Screen name="TerceiraTela" component={TerceiraTela}/>
        <Stack.Screen name="QuartaTela" component={QuartaTela}/>
        <Stack.Screen name="QuintaTela" component={QuintaTela}/>
          
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  

});
