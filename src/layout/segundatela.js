import { useState, useEffect} from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";

export default function SegundaTela() {
  const [count, setCount] = useState(0);

  return (
    <View style={styles.container}>

      <Text> Clique para contar: {count} </Text>
      <TouchableOpacity
        style = {styles.teste}
        onPress={() => setCount(count + 1)}
      >
      <Text>Clique!</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#c2dbf0',
    alignItems: 'center',
    justifyContent: 'center',
  },

  teste: {
    color: "black",
    backgroundColor: "pink",
    width: 90,
    borderRadius: 25,
    alignItems: "center"
  },
});
