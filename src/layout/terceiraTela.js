import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView } from 'react-native';

export default function   TerceiraTela() {
  return (

    <View style={styles.container}>
      <View style={styles.header}>
        <Text>MENU</Text>
      </View>

      <View style={styles.content}>
        <ScrollView>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        <Text style={styles.text}>Hello World!</Text>
        </ScrollView>
      </View>

      <View style={styles.footer}>
        <Text>RODAPE</Text>
      </View>
      </View>
  );
}
                                                    
const styles = StyleSheet.create({
  container: {
    flex: 1,
    FlexDirection: "column",   
    justifyContent: "space-between",
  },
  header:{
    height:50,
    backgroundColor:'blue',
  },
  content:{
    flex:1,
    backgroundColor:'green',
  },
  footer:{
    height:50,
    backgroundColor:'red',  
  },
  text:{
    frontSize:16,
    fontWeight: 'bold',
    textAlign: 'center',
  }
});

