import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import { useEffect, useState } from 'react';

export default function TelaInicial() {
  const [text, setText] = useState("");
  const[numLetras, setNumLetras] = useState(0);
  function click(){
    setNumLetras(text.length)
  }
  return (
    <View style={styles.container}>
      <Text style={styles.text}> Para contar caracteres escreva aqui: {text} </Text>
      <TextInput
        style = {styles.input}
        placeholder='Digite algo aqui...'
        value={text}
        onChangeText={(textInput) => setText(textInput)}
      />
      <Button
        title='Clique'
        onPress={() => {
          click();
        }}
      />
      {
        numLetras > 0 ?
        <Text> {numLetras} </Text>
        :
        <Text></Text>
      }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#c2dbf0',
    alignItems: 'center',
    justifyContent: 'center',
  },

  text: {
    alignItems: "center",
    fontSize: 20,
    fontWeight: 'bold'
  },
  input: {
    borderWidth: 1,
    borderColor: 'pink',
    width: '80%',
    padding: 10,
    marginVertical: 10,
    backgroundColor: 'white',
  }
});

//Open up App.js to start working on your app!